package com.jcasey.projection;

import com.jcasey.projection.R;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final DrawView drawView = (DrawView)findViewById(R.id.drawView1);
		final SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
		final TextView textAngle = (TextView)findViewById(R.id.textAngle);
		
		drawView.setSeekBar(seekBar); // expose the seekBar to the DrawView so we can update progress as we render
		
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				drawView.postDelayed(new Runnable(){
					@Override
					public void run() {
						drawView.manual = false;
						drawView.invalidate();
					}
				}, 2500);
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				drawView.manual = true;
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
		
				drawView.setAngle(progress);
				drawView.invalidate();
				textAngle.setText(""+progress);
			}
		});
		
	}
}
