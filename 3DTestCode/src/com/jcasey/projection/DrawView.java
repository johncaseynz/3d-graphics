package com.jcasey.projection;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;

public class DrawView extends View {
	private static int SCALE = 400;
	SeekBar seekBar;
	Paint paint = new Paint();
	Vec3 points[] = new Vec3[8];
	int angle = 0;
	boolean manual = false;

	int[][] faces = new int[4][]; 

	float [] cos = new float[360];
	float [] sin = new float[360];

	public final float distance = 3.5f; // Everything needs to be in the same units. This is in the same units as the cube.

	class Vec3
	{
		float x;
		float y;
		float z;

		public Vec3()
		{

		}

		public Vec3(float x, float y, float z) {
			super();
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public final Vec3 rotateX(double cosa, double sina)
		{


			Vec3 rotated = new Vec3();

			rotated.x = x;
			rotated.y = (float) (y * cosa - z * sina);
			rotated.z = (float) (y * sina + z * cosa);

			return rotated;
		}

		public final Vec3 rotateY(double cosa, double sina)
		{		
			Vec3 rotated = new Vec3();

			rotated.x = (float)(z * sina + x * cosa);
			rotated.y = y;
			rotated.z = (float) (z * cosa - x * sina);

			return rotated;
		}

		public final Vec3 rotateZ(double cosa, double sina)
		{		
			Vec3 rotated = new Vec3();

			rotated.x = (float)( x * cosa - y * sina);
			rotated.y = (float) (x * sina + y * cosa);
			rotated.z = z;

			return rotated;
		}

		public PointF project_point()
		{
			float cosa = cos[angle];
			float sina = sin[angle];

			Vec3 rotated;
			if(angle <90)
			{
				rotated = rotateX(cosa,sina);				
			}
			else if(angle < 180)
			{
				rotated = rotateY(cosa,sina);
			}
			else if(angle < 270)
			{
				rotated = rotateX(cosa,sina);
			}
			else
			{
				rotated = rotateX(cosa,sina).rotateY(cosa,sina).rotateZ(cosa,sina);
			}

			float xCartesian = project(rotated.x, rotated.z);
			float yCartesian = project(rotated.y, rotated.z);

			float screenX = cartesianXToScreenX(xCartesian);
			float screenY = cartesianYToScreenY(yCartesian);

			return new PointF(screenX, screenY);
		}

		public final float project(final float pointWorld,final float zWorld)
		{
			float z = (zWorld + distance); // the distance from the origin
			return ((distance * pointWorld) / z) * SCALE;
		}
		public void displayDebug(Canvas canvas, PointF screen)
		{
			canvas.drawText(String.format("%.2f",x)+","+String.format("%.2f",y)+","+String.format("%.2f",z), screen.x + 15, screen.y + 15, paint);
			canvas.drawText(String.format("%.2f",screen.x)+","+String.format("%.2f",screen.y), screen.x + 15, screen.y + 30, paint);
		}
	}

	public DrawView(Context context) {
		super(context);

		setup();
	}

	public DrawView(Context context, AttributeSet attrs) {
		super(context, attrs);

		setup();
	}

	public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);

		setup();
	}

	public void setup()
	{		

		for(int i = 0; i <cos.length ;i++)
		{
			cos[i] = (float) Math.cos(Math.toRadians(i));
			sin[i] = (float) Math.sin(Math.toRadians(i));

		}

		points[0] = new Vec3(1,1,1);
		points[1] = new Vec3(1,1,-1);
		points[2] = new Vec3(1,-1,-1);
		points[3] = new Vec3(1,-1,1); //
		points[4] = new Vec3(-1,-1,1);
		points[5] = new Vec3(-1,-1,-1);
		points[6] = new Vec3(-1,1,-1);
		points[7] = new Vec3(-1,1,1); //

		// only draw 4 faces because of line overlaps
		faces[0] = new int[] {0,1,2,3}; // right
		faces[1] = new int[] {0,1,6,7}; // top
		faces[2] = new int[] {6,7,4,5}; // left
		faces[3] = new int[] {4,5,2,3}; // bottom

		//		paint.setAntiAlias(true);
	}


	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);

		paint.setColor(Color.RED);

		for(int index = 0; index < faces.length; index++)
		{
			for (int j = 0; j< faces[index].length; j++)
			{
				Vec3 point0 = points[faces[index][0]];
				Vec3 point1 = points[faces[index][1]];
				Vec3 point2 = points[faces[index][2]];
				Vec3 point3 = points[faces[index][3]];

				PointF ptn0 = point0.project_point();
				PointF ptn1 = point1.project_point();
				PointF ptn2 = point2.project_point();
				PointF ptn3 = point3.project_point();

				point0.displayDebug(canvas,ptn0);
				point1.displayDebug(canvas,ptn1);
				point2.displayDebug(canvas,ptn2);
				point3.displayDebug(canvas,ptn3);
				
				canvas.drawLine(ptn0.x, ptn0.y, ptn1.x, ptn1.y, paint);		
				canvas.drawLine(ptn1.x, ptn1.y, ptn2.x,ptn2.y, paint);
				canvas.drawLine(ptn2.x,ptn2.y, ptn3.x,ptn3.y, paint);
				canvas.drawLine(ptn3.x, ptn3.y, ptn0.x, ptn0.y, paint);
			}
		}



		if(!manual)
		{
			if(seekBar != null)
			{
				seekBar.setProgress(angle);
			}

			setAngle(angle + 1);

			if(angle == 360)
			{ 
				angle = 0;
			}
			
			invalidate();
		}
	}

	public final float cartesianXToScreenX(final float cartesianX)
	{
		return (cartesianX ) + (getWidth()/2);
	}

	public final float cartesianYToScreenY(final float cartesianY)
	{
		return (getHeight()/2) - (cartesianY );
	}

	public void setAngle(int progress) {
		this.angle = progress;

		
	}

	public void setSeekBar(SeekBar seekBar) {
		this.seekBar = seekBar;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		
		double hypot = Math.hypot(w, h);
		
		if(w > h)
		{
			SCALE = (int) (hypot / 8); // we have to do a little more scaling if we are in landscape mode
		}
		else if(h > w)
		{
			SCALE = (int) (hypot / 6);
		}
	}
}
